Thanks to Kilon Alios for writing the original C++ shared memory bridge included 
as a dependency and directly available here: 
https://github.com/kilon/CPP

This project is for Godot Mono 3.2.3.

To run, open the project in Godot Mono 3.2.3 and hit play in the top right
corner. A window with a floating box will appear. Open the included 
GodotMemoryServer.image with the Pharo launcher and 3 lines will be highlighted.
Press ctrl + D on those 3 lines and Pharo will connect to the exposed memory of
Godot and the cube will be affected by gravity. You can move with the arrow keys
and jump with spacebar (although I didn't code a "grounded" check, so really the
cube can just fly forever, but good enough for a proof of concept).

I'm confident an entire game can be coded using this method, however since it
relies on being able to run the PharoVM in parallel with the game, it limits the
game to desktop only, which is not what I wanted. Because of this, I started to
write my own custom VM and library that exports to my arbitrary bytecode instead,
however I'm uploading this for any purist who wants an example of how one could
code a game to use Pharo, any other Smalltalk, or really most any other language
that can handle shared memory directly as a scripting language by exposing traits
through a memory map.
