using Godot;
using System;
using System.IO.MemoryMappedFiles;
using System.Runtime.InteropServices;
using System.Text;

public class SmalltalkProxy : Node
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	public MemoryMappedFile mmf;
	public MemoryMappedViewAccessor accessor;
	public byte bZero = 0;
	public byte bOne = 1;

	public KinematicBody player;
	public Vector3 playerMovement;
	public float[] playerMoveArr;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		mmf = MemoryMappedFile.CreateOrOpen("Smalltalk2", 1000);
		//mmf = MemoryMappedFile.CreateFromFile(@"C:\Users\chand\Documents\MemMapTest\other3", System.IO.FileMode.Open, "Smalltalk2");
		accessor = mmf.CreateViewAccessor();
		GD.Print(accessor.ReadByte(2));

		player = (KinematicBody)GetNode("boiKinematic");
		playerMoveArr = new float[3];
	}

	  // Called every frame. 'delta' is the elapsed time since the previous frame.
	  public override void _Process(float delta)
	  {
		accessor.ReadArray<float>(0, playerMoveArr, 0, 3);
		playerMovement.x = playerMoveArr[0] * delta;
		playerMovement.y = playerMoveArr[1] * delta;
		playerMovement.z = playerMoveArr[2] * delta;
		player.MoveAndSlide(playerMovement);

		accessor.Write(12, (byte)0);
		accessor.Write(13, (byte)0);
		accessor.Write(14, (byte)0);
		accessor.Write(15, (byte)0);
		accessor.Write(16, (byte)0);

		if (Input.IsActionPressed("ui_right"))
		{
			accessor.Write(12, (byte)1);
		}
		if (Input.IsActionPressed("ui_left"))
		{
			accessor.Write(13, (byte)1);
		}
		if (Input.IsActionPressed("ui_down"))
		{
			accessor.Write(14, (byte)1);
		}
		if (Input.IsActionPressed("ui_up"))
		{
			accessor.Write(15, (byte)1);
		}
		if (Input.IsActionPressed("ui_accept"))
		{
			accessor.Write(16, (byte)1);
		}
	}

	public override void _Notification(int what)
	{
		if (what == MainLoop.NotificationWmQuitRequest)
		{
			accessor.Dispose();
			mmf.Dispose();
			GetTree().Quit(); // default behavior
		}
	}

}
